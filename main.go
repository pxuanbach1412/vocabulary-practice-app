package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("Hello World")

	var name = "Vocabulary Practice App"
	fmt.Println(name)
	fmt.Println("Welcome to", name, "!")

	const conferenceTickets = 50
	fmt.Println("We have total of", conferenceTickets, "tickets")

	const remainTickets = 49
	fmt.Printf("We have total of %v are still available.\n", remainTickets)

	var userName string
	var userTickets int

	fmt.Println("Enter your name:")
	fmt.Scan(&userName)

	// userName = "Pham Xuan Bach"
	userTickets = 2
	fmt.Printf("User %v booked %v tickets.\n", userName, userTickets)
	fmt.Printf("User %T booked %T tickets.\n", userName, userTickets)

	var bookings [50]string
	bookings[0] = "Kafka " + userName
	bookings[1] = "Seele " + userName
	bookings[2] = "Natasha " + userName

	fmt.Printf("Your bookings array %v\n", bookings)
	fmt.Printf("The first booking is %v\n", bookings[0])
	fmt.Printf("Array type %T\n", bookings)
	fmt.Printf("Array lenght %v\n", len(bookings))

	// what if we don't know  the size when creating it?
	// A list that more dynamic in size?
	// => Slices (abstraction of an Array)
	listings := []string{}
	listings = append(listings, bookings[0])
	listings = append(listings, bookings[1])

	fmt.Printf("Your listings array %v\n", listings)
	fmt.Printf("The first listing is %v\n", listings[0])
	fmt.Printf("Array type %T\n", listings)
	fmt.Printf("Array lenght %v\n", len(listings))

	// Only have for - loop
	for {
		var forValue string

		fmt.Println("Enter for value:")
		fmt.Scan(&forValue)

		fmt.Printf("Current for value is %v.\n", forValue)

		// ignore index declared and not used => change index to _
		for _, listing := range listings {
			var listingName = strings.Fields(listing)
			var firstName = listingName[0]

			// fmt.Printf("In for - loop, index %v and first name %v.\n", index, firstName)
			fmt.Printf("In for - loop, first name is %v.\n", firstName)
		}

		if forValue == "stop" {
			// end loop
			fmt.Println("End of for - loop.")
			break
		}

		// learnVocabulary(forValue)
		practiceVocabulary(forValue)
	}
}

func learnVocabulary(value string) {
	switch value {
	case "hello", "hi":
		fmt.Println("xin chao.")
	case "work":
		fmt.Println("cong viec.")
	default:
		fmt.Println("Khong biet.")
	}
}

func practiceVocabulary(value string) {
	// var vocabularyMap map[string]string
	const maxVocabulary uint = 50
	var vocabularyMap = make(map[string]string)

	vocabularyMap["hello"] = "xin chao"
	vocabularyMap["hi"] = "xin chao"
	vocabularyMap["work"] = "cong viec"
	vocabularyMap["surpass"] = "vuot qua"
	vocabularyMap["object"] = "doi tuong"

	vocabularyMap["maxlength"] = strconv.FormatUint(uint64(maxVocabulary), 10)

	fmt.Println(vocabularyMap[value])
}

type Vocabulary struct {
	word  string
	mean  string
	level int
}

func practiceVocabularyStruct(value string) {
	var vocabularys = make([]Vocabulary, 0)

	var work = Vocabulary{
		word:  "work",
		mean:  "cong viec",
		level: 1,
	}

	vocabularys = append(vocabularys, work)

	for _, vocabulary := range vocabularys {
		fmt.Printf("The word '%v' mean %v.\n", vocabulary.word, vocabulary.mean)
	}
}
